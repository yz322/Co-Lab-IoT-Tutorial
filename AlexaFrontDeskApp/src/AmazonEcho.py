import logging
from flask import Flask, render_template
from flask_ask import Ask, statement, question
from sqlalchemy import *
from sqlalchemy.orm import sessionmaker
from datetime import datetime
from OurServices import OfficeHour, RootsClass, Base, compare_roots_class

app = Flask(__name__)
ask = Ask(app, '/')

logging.getLogger('flask_ask').setLevel(logging.DEBUG)

Session = sessionmaker()
db = create_engine('sqlite:///data.sqlite')
Base.metadata.create_all(db)
Session.configure(bind=db)
session = Session()


@ask.launch
def launch_intent():
    card_title = render_template('welcome_title')
    speech_response = render_template('welcome_speech')
    card_text = render_template('welcome_speech')
    reprompt_speech = render_template('default_reprompt_speech')
    return question(speech_response).reprompt(reprompt_speech).simple_card(card_title, card_text)


@ask.intent('GeneralInfo')
def general_info_intent():
    card_title = render_template('about_title')
    speech_response = render_template('about_speech')
    card_text = render_template('about_card_text')
    return statement(speech_response).simple_card(card_title, card_text)


@ask.intent('Hours')
def hours_intent():
    card_title = render_template('hours_title')
    speech_response = render_template('hours_speech')
    return statement(speech_response).simple_card(card_title, speech_response)


@ask.intent('OfficeHoursGeneral')
def hours_intent():
    card_title = render_template('office_hours_title')
    speech_response = render_template('office_hours_speech')
    card_text = render_template('office_hours_text')
    return statement(speech_response).simple_card(card_title, card_text)


@ask.intent('RootsClassGeneral')
def roots_class_intent():
    card_title = render_template('roots_class_title')
    speech_response = render_template('roots_class_speech')
    card_text = render_template('roots_class_text')
    return statement(speech_response).simple_card(card_title, card_text)


@ask.intent('DukeCardOffice')
def duke_card_office_intent():
    card_title = render_template('office_hours_title')
    speech_response = render_template('duke_card_office_speech')
    return statement(speech_response).simple_card(card_title, speech_response)


@ask.intent('AskForTaByKind', default={'tech': ''})
def AskForTaByKind_intent(tech):
    if tech == '':
        reprompt_speech = render_template('default_reprompt')
        reprompt_title = render_template('default_reprompt_title')
        reprompt_text = render_template('default_reprompt_text')
        return question(reprompt_speech).simple_card(reprompt_title, reprompt_text)
    tech = tech.lower()
    ta = session.query(OfficeHour).filter(OfficeHour.tags.like('%' + tech + '%')).first()
    if ta is None:
        response_speech = render_template('ta_not_found_speech', tech=tech)
        card_title = render_template('ta_not_found_title')
        return statement(response_speech).simple_card(card_title, response_speech)
    else:
        if ta.gender == 'male':
            gender = 'He'
        else:
            gender = 'She'
        response_speech = render_template('ta_specialty_speech', tech=tech, name=ta.name, gender=gender, time=ta.times)
        card_title = render_template('ta_found_title')
        return statement(response_speech).simple_card(card_title, response_speech)


@ask.intent('RootsClassByKind', default={'tech': ''})
def roots_class_by_kind_intent(tech):
    if tech == '':
        reprompt_speech = render_template('default_reprompt')
        reprompt_title = render_template('default_reprompt_title')
        reprompt_text = render_template('default_reprompt_text')
        return question(reprompt_speech).simple_card(reprompt_title, reprompt_text)
    tech = tech.lower()
    class_list = session.query(RootsClass).filter(RootsClass.tags.like('%' + tech + '%')).all()
    class_list = [i for i in class_list if i.is_future()]
    if not class_list:
        response_speech = render_template('roots_class_not_found_speech', tech=tech)
        card_title = render_template('roots_class_not_found_title')
        return statement(response_speech).simple_card(card_title, response_speech)
    else:
        class_list = sorted(class_list, cmp=compare_roots_class)
        roots_class = class_list[0]
        datetime_obj = datetime.strptime(roots_class.times, '%Y-%m-%d %I:%M %p')
        day_in_week = datetime_obj.strftime('%A')
        date = datetime_obj.strftime('%m%d')
        hour = datetime_obj.strftime('%I') + ' '
        minutes = datetime_obj.strftime('%M')
        if minutes == '00':
            minutes = ''
        else:
            minutes += ' '
        part_of_the_day = datetime_obj.strftime('%p')
        if part_of_the_day == 'AM':
            part_of_the_day = 'a.m.'
        else:
            part_of_the_day = 'p.m.'
        time = hour + minutes + part_of_the_day
        date_time_str = datetime_obj.strftime('%A %B %d, %I:%M %p')
        response_speech = render_template('roots_class_found_speech', tech=tech, instructor=roots_class.instructor,
                                          date=date, time=time, day_in_week=day_in_week)
        card_title = render_template('roots_class_found_title')
        card_text = render_template('roots_class_found_card_text', tech=tech, date_time_str=date_time_str,
                                    instructor=roots_class.instructor)
        return statement(response_speech).simple_card(card_title, card_text)


@ask.intent('AskForTaName', default={'name': ''})
def filter_by_consultant_name_intent(name):
    if name == '':
        card_title = render_template('default_reprompt_title')
        card_text = render_template('default_reprompt_text')
        reprompt_speech = render_template('default_reprompt_speech')
        return question(reprompt_speech).simple_card(card_title, card_text)
    name = name.lower()
    ta = session.query(OfficeHour).filter(OfficeHour.name.like('%' + name + '%')).first()
    if ta is None:
        response_speech = render_template('ta_name_not_found_speech')
        card_title = render_template('ta_not_found_title')
        return statement(response_speech).simple_card(card_title, response_speech)
    else:
        if ta.gender == 'male':
            he_or_she = 'he'
            his_or_her = 'his'
        else:
            he_or_she = 'she'
            his_or_her = 'her'
        response_speech = render_template('ta_name_speech', name=ta.name, he_or_she=he_or_she,
                                          his_or_her=his_or_her, time=ta.times, tech=ta.tags)
        card_title = render_template('ta_found_title')
        return statement(response_speech).simple_card(card_title, response_speech)


@ask.intent('TechSupportByKind', default={'tech': ''})
def tech_support_by_kind_intent(tech):
    ta = session.query(OfficeHour).filter(OfficeHour.tags.like('%' + tech + '%')).first()
    class_list = session.query(RootsClass).filter(RootsClass.tags.like('%' + tech + '%')).all()
    class_list = [i for i in class_list if i.is_future()]
    if (ta is None) and (not class_list):
        card_title = render_template('default_reprompt_title')
        card_text = render_template('default_reprompt_text')
        reprompt_speech = render_template('support_by_kind_not_found_speech')
        return question(reprompt_speech).simple_card(card_title, card_text)
    elif (ta is not None) and (not class_list):
        return AskForTaByKind_intent(tech)
    elif (ta is None) and class_list:
        return roots_class_by_kind_intent(tech)
    else:
        if ta.gender == 'male':
            gender = 'He'
        else:
            gender = 'She'
        class_list = sorted(class_list, cmp=compare_roots_class)
        roots_class = class_list[0]
        datetime_obj = datetime.strptime(roots_class.times, '%Y-%m-%d %I:%M %p')
        day_in_week = datetime_obj.strftime('%A')
        date = datetime_obj.strftime('%m%d')
        hour = datetime_obj.strftime('%I') + ' '
        minutes = datetime_obj.strftime('%M')
        if minutes == '00':
            minutes = ''
        else:
            minutes += ' '
        part_of_the_day = datetime_obj.strftime('%p')
        if part_of_the_day == 'AM':
            part_of_the_day = 'a.m.'
        else:
            part_of_the_day = 'p.m.'
        time = hour + minutes + part_of_the_day
        date_time_str = datetime_obj.strftime('%A %B %d, %I:%M %p')
        response_card_title = render_template('support_by_kind_found_title')
        response_speech = render_template('support_by_kind_found_speech', tech=tech, ta_name=ta.name, gender=gender,
                                          ta_time=ta.times, class_name=roots_class.name, day_in_week=day_in_week,
                                          date=date, time=time, instructor=roots_class.instructor)
        response_card_text = render_template('support_by_kind_found_text', tech=tech, ta_name=ta.name, gender=gender,
                                             ta_time=ta.times, class_name=roots_class.name, date_time_str=date_time_str,
                                             instructor=roots_class.instructor)
        return statement(response_speech).simple_card(response_card_title, response_card_text)


@ask.intent('AMAZON.StopIntent')
def stop():
    return statement(render_template('stop_speech'))


@ask.intent('AMAZON.CancelIntent')
def cancel():
    return statement(render_template('stop_speech'))


@ask.intent('AMAZON.HelpIntent')
def help_intent():
    card_title = render_template('welcome_title')
    speech_response = render_template('welcome_speech')
    card_text = render_template('welcome_speech')
    reprompt_speech = render_template('default_reprompt')
    return question(speech_response).reprompt(reprompt_speech).simple_card(card_title, card_text)


@ask.session_ended
def session_ended():
    return "{}", 200


if __name__ == '__main__':
    context = ('certificate.pem', 'private-key.pem')
    app.run(ssl_context=context, threaded=True)
