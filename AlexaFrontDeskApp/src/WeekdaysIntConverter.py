# This class is used as enum.
class DaysInWeek(object):
    sunday, monday, tuesday, wednesday, thursday, friday, saturday = range(7)
    sun, mon, tue, wed, thu, fri, sat = range(7)
    sun, mon, tues, wed, thurs, fri, sat = range(7)


class WeekdaysIntConverter(object):
    @staticmethod
    def convert(weekday_string):
        """ Convert weekdays from English word into int.

        Convert weekdays from English word into int. Starts with sunday (0).

        :rtype: int
        :param str weekday_string: weekday in English words.
        :return: weekdays in integer. Starts with Sunday (0).
        """
        weekday_string = weekday_string.lower()
        result = -1
        try:
            result = getattr(DaysInWeek, weekday_string)
        except AttributeError:
            pass
        return result


