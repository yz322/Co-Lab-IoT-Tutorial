from sqlalchemy import *
from sqlalchemy.ext.declarative import declarative_base
from datetime import datetime

Base = declarative_base()


class CoLabService(Base):
    __tablename__ = 'CoLabServices'
    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String)
    times = Column(String)
    tags = Column(String)
    type = Column(String)

    __mapper_args__ = {
        'polymorphic_on': type,
        'polymorphic_identity': 'CoLabService'
    }

    def __init__(self, name, times='', tags=''):
        self.name = name
        self.times = times
        self.tags = tags


class OfficeHour(CoLabService):
    # This info is needed to distinguish he/she in the response.
    gender = Column(String)
    __mapper_args__ = {
        'polymorphic_identity': 'OfficeHour'
    }

    def __init__(self, name, times='', tags='', gender=''):
        super(OfficeHour, self).__init__(name, times, tags)
        self.gender = gender


class RootsClass(CoLabService):
    instructor = Column(String)
    id_from_api = Column(Integer)
    __mapper_args__ = {
        'polymorphic_identity': 'RootsClass'
    }

    def __init__(self, name, times='', tags='', instructor='', id_from_api=-1):
        super(RootsClass, self).__init__(name, times, tags)
        self.instructor = instructor
        if id_from_api is not -1:
            self.id_from_api = id_from_api
        else:
            self.id_from_api = None

    def is_future(self):
        try:
            datetime_obj = datetime.strptime(self.times, '%Y-%m-%d %I:%M %p')
        except ValueError:
            return false

        if datetime.now() < datetime_obj:
            return true
        else:
            return false


def compare_roots_class(x, y):
    x_time = datetime.strptime(x.times, '%Y-%m-%d %I:%M %p')
    y_time = datetime.strptime(y.times, '%Y-%m-%d %I:%M %p')

    if x_time < y_time:
        return -1
    elif x_time > y_time:
        return 1
    else:
        return 0


class RootsClassInstructor(Base):
    __tablename__ = 'RootsClassInstructors'
    id = Column(Integer, primary_key=True, autoincrement=True)
    id_from_api = Column(Integer)
    name = Column(String)

    def __init__(self, id_from_api=-1, name=''):
        self.name = name
        if id_from_api is not -1:
            self.id_from_api = id_from_api
        else:
            self.id_from_api = None
