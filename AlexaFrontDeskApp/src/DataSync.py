import requests
import logging
import time
import schedule
from sqlalchemy import *
from sqlalchemy.orm import sessionmaker
from datetime import datetime

from OurServices import CoLabService, RootsClass, RootsClassInstructor, Base

BASE_URL = 'https://training.oit.duke.edu/enroll/api/index.php'
CLASSES_PATH = '/events/colab/10'
INSTRUCTOR_PATH = '/users/id/'
KEY = 'LuCi9t81kcLWGP558l47xD1xH072574t'

logging.getLogger(__name__).setLevel(logging.INFO)

Session = sessionmaker()
db = create_engine('sqlite:///data.sqlite')
Base.metadata.create_all(db)
Session.configure(bind=db)
session = Session()


def update_classes():
    global session
    try:
        classes = requests.get(BASE_URL + CLASSES_PATH, params={'key': KEY})
        if classes.status_code == 200:
            classes = classes.json()
        else:
            raise ValueError('Cannot connect to OIT API')
    except Exception as e:
        logging.exception(e)
        return
    session.query(CoLabService).filter(CoLabService.type == 'RootsClass').delete()
    for key, value in classes.iteritems():
        if value['status'] == 'open':
            new_class = create_new_class(key, value)
            if new_class is not None:
                session.add(new_class)


def create_new_class(default_id=-1, content=None):
    if content is None:
        return
    try:
        instructor_name = search_instructor_name_by_id(content['facilitators'][0])
    except Exception as e:
        logging.exception(e)
        return
    tags = ','.join(content['subjects'])
    for i in content['dates']:
        if i['id'] is None:
            i['id'] = default_id
            datetime_obj = datetime.strptime(i['time1'], '%Y-%m-%d %H:%M:%S')
            new_class = RootsClass(content['title'], datetime_obj.strftime('%Y-%m-%d %I:%M %p'), tags,
                                   instructor_name, i['id'])
            return new_class


def search_instructor_name_by_id(id_from_api):
    global session, BASE_URL, INSTRUCTOR_PATH, KEY
    instructor = session.query(RootsClassInstructor).filter_by(id_from_api=id_from_api).first()
    if instructor is not None:
        return instructor.name
    else:
        response = requests.get(BASE_URL + INSTRUCTOR_PATH + id_from_api, params={'key': KEY}).json()
        new_instructor_name = response[0]['firstname'] + ' ' + response[0]['lastname']
        new_instructor = RootsClassInstructor(id_from_api, new_instructor_name)
        session.add(new_instructor)
        return new_instructor_name


def job():
    logging.info('Update is in progress at ' + datetime.now().strftime('%Y-%m-%d %I:%M %p') + '\n')
    try:
        update_classes()
    except Exception as e:
        logging.exception(e)
        session.rollback()
    session.commit()
    return


schedule.every().day.at('03:30').do(job)

while True:
    schedule.run_pending()
    time.sleep(1)
