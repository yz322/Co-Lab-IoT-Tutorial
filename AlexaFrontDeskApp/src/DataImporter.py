import logging
from datetime import datetime

from sqlalchemy import *
from sqlalchemy.orm import sessionmaker

from OurServices import CoLabService, OfficeHour, RootsClass, Base

logging.getLogger(__name__).setLevel(logging.DEBUG)


def add_or_update(new_obj):
    global session
    old_obj = session.query(CoLabService).filter_by(name=new_obj.name).first()
    if old_obj is not None:
        old_obj.times = new_obj.times
        old_obj.tags = new_obj.tags
        old_obj.gender = new_obj.gender
    else:
        session.add(new_obj)
    session.commit()


Session = sessionmaker()
db = create_engine('sqlite:///data.sqlite')
Base.metadata.create_all(db)
Session.configure(bind=db)
session = Session()

while True:
    continue_command = raw_input('Do you want to change Office hours? Y or y for continue\n')
    if continue_command.lower() != 'y':
        break
    name = raw_input('Type the TA''s name\n')
    time_slots = raw_input('The office hours time slot, in natural language\n')
    tags = raw_input('Type in specialties, seperate by comma\n')
    tags = tags.split(',')
    tags = [tag.strip(' ') for tag in tags]
    tags = ','.join(tags)
    gender = raw_input('Type the gender of the TA. This is needed as we need to distinguish he/she in response.\n')
    new_ta = OfficeHour(name, time_slots, tags, gender)
    add_or_update(new_ta)

while True:
    continue_command = raw_input('Do you want to change Roots Classes? Y or y for continue\n')
    if continue_command.lower() != 'y':
        break
    name = raw_input('Type the class name\n')
    time_slots = raw_input('The time, in %Y-%m-%d %I:%M %p\n')
    try:
        datetime_obj = datetime.strptime(time_slots, '%Y-%m-%d %I:%M %p')
    except Exception:
        print 'Format incorrect!'
        continue
    tags = raw_input('Type in tags, seperate by comma\n')
    instructor_name = raw_input('Type in instructor name\n')
    tags = tags.split(',')
    tags = [tag.strip(' ') for tag in tags]
    tags = ','.join(tags)
    new_class = RootsClass(name, time_slots, tags, instructor_name)
    session.add(new_class)
    session.commit()

session.flush()
session.close()
