import datetime
import time
import json


class Record:
    numberOfAps = 0
    numberOfDevices = 0
    timeStamp = ''
    sessionKey = 'colab'

    def __init__(self, ap_num, device_num):
        self.numberOfAps = ap_num
        self.numberOfDevices = device_num
        self.timeStamp = datetime.datetime.fromtimestamp(time.time()) \
            .strftime('%Y-%m-%d %H:%M:%S')
        self.sessionKey = 'colab'

    def __str__(self):
        return json.dumps(self.__dict__)

    @staticmethod
    def init_json(json_data):
        new_obj = Record(0, 0)
        new_obj.__dict__ = json_data
        new_obj.sessionKey = 'colab'
        return new_obj
