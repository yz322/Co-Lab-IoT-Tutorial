import json
import sqlite3
from collections import deque
from flask import Flask, request, Response, g

from record import Record

app = Flask(__name__)
queue = deque(maxlen=100)


@app.route('/post/', methods=['POST'])
def handle_post():
    global queue
    try:
        decoded_json = request.get_json(force=True)
    except ValueError:
        return 'The data format is not correct', 412
    if decoded_json['sessionKey'] != 'colab':
        return 'sessionKey is not correct', 403
    result = Record.init_json(decoded_json)
    try:
        write_to_file(result)
    except IOError as io_e:
        print io_e
        return 'Cannot write to file', 500
    queue.append(result)
    return 'Data accepted'


@app.route('/all/', methods=['GET'])
def handle_all():
    if len(queue) > 0:
        result = json.dumps([i.__dict__ for i in queue])
        header = {'Access-Control-Allow-Origin': '*'}
        resp = Response(result, headers=header)
        return resp
    else:
        return 'No data available', 404


@app.route('/mostrecent/', methods=['GET'])
def handle_most_recent():
    if len(queue) > 0:
        result = json.dumps(queue[-1].__dict__)
        header = {'Access-Control-Allow-Origin': '*'}
        resp = Response(result, headers=header)
        return resp
    else:
        return 'No data available', 404


def write_to_file(data):
    with open('data/' + data.sessionKey + '.txt', 'a+') as f:
        f.write(data.__str__())
        f.write('\n')
        f.close()


if __name__ == "__main__":
    try:
        f = open('data/colab.txt')
        content = f.readlines()
        content = [x.strip('\n') for x in content]
        for line in content:
            result = Record.init_json(json.loads(line))
            queue.append(result)
    except Exception as file_e:
        print file_e

    app.run(host="0.0.0.0", port=5001, threaded=True)
