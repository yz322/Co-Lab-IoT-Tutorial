import json
import subprocess
import time

import requests

from record import Record

PACKETS_PER_TRY = 2000
ap_list = {}


def listen():
    global ap_list
    devices = {}
    listen_process = subprocess.Popen(['tcpdump', '-I', '-i', 'en0', '-p', '-e', '-c',
                                       str(PACKETS_PER_TRY)], stdout=subprocess.PIPE)
    for row in iter(listen_process.stdout.readline, b''):
        line = row.rstrip()
        if 'Beacon' in line:
            start_loc = line.find('BSSID:')
            bssid = None
            name = None
            if start_loc is not -1:
                bssid = line[start_loc + 6:start_loc + 23]
            start_loc = line.find('Beacon (')
            end_loc = line.find(')', start_loc, len(line))
            if (start_loc is not -1) and (end_loc is not -1):
                name = line[start_loc + 8:end_loc]
            if (bssid is not None) and (name is not None):
                ap_list[bssid] = name
        elif 'TA' in line:
            start_loc = line.find('TA:')
            mac_addr = line[start_loc + 3:start_loc + 20]
            end_loc = line.find('dB signal')
            start_loc = line.find('-', end_loc - 5, end_loc + 2)
            signal = line[start_loc: end_loc]
            devices[mac_addr] = signal

    try:
        listen_process.terminate()
    except OSError as os_error:
        print os_error

    for key in ap_list:
        if key in devices:
            del devices[key]

    return Record(len(ap_list), len(devices))


def send(data):
    payload = json.dumps(data.__dict__)
    url = 'http://0.0.0.0:5001/post/'
    head = {'Content-Type': 'application/json'}
    response = requests.post(url, headers=head, data=payload)
    if not response.ok:
        raise ValueError('Wrong response!')


start = time.time()
while True:
    end = time.time()
    if (end - start) > 1:
        try:
            data = listen()
            time.sleep(3)
            send(data)
        except Exception as e:
            print e
        start = time.time()
